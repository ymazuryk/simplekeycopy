﻿using Caliburn.Micro;
using SimpleKeyCopy.Proxmark;
using SimpleKeyCopy.Proxmark.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SimpleKeyCopy.ViewModels
{
    /// <summary>
    /// Test Key - Step 1
    /// </summary>
    public class IdentifyKeyStepViewModel: StepViewModel
    {
        public IdentifyKeyStepViewModel(IEventAggregator eventAggregator): base(0, eventAggregator)
        {
        }

        protected override void OnActivate()
        {
            this.KeyInformation = null;
            logger.Trace("Identify key step activated");
        }
        public async void OnSearchPressed()
        {
            // pass filename as an arugment for testing         
            var key = await ProxmarkManager.Instance.LfSearch();
            if (key == null)
            {
                MessageBox.Show("No Key found, please try again", "No Key Found!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            
            var validationResult = KeyValidator.Create(key.KeyType).ValidateKey(key);
            if (!validationResult.IsValid)
            {
                var validationErrors = String.Join("\n", validationResult.ValidationErrors);
                if (MessageBox.Show($"Key might have been read incorrectly:\n {validationErrors}\n Woud you like to continue?",
                    "Found Key Information Might Be Corrupt", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                {
                    MessageBox.Show("Remove the key and place it on a reader again\nPress Ok when ready.", "Reposition the Key", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.OnActivate();
                    return;
                }
            }
            this.KeyInformation = key;
            logger.Trace($"Found key {key}");
            PublishStepFinishedEvent();
        }

        public ProxmarkKeyInformation KeyInformation { get; set; }
    }
}
