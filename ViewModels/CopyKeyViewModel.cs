﻿using Caliburn.Micro;
using SimpleKeyCopy.Events;
using SimpleKeyCopy.Tracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleKeyCopy.ViewModels
{
    public class CopyKeyViewModel : Conductor<StepViewModel>.Collection.OneActive, IHandle<StepFinishedEvent>, IHandle<CreateAnotherDuplicateEvent
        >
    {
        private IEventAggregator eventAggregator;
        private int selectedStep;
        private int numberOfCopies = 0;
        private IdentifyKeyStepViewModel identifyKeyViewModel;
        private WriteKeyStepViewModel writeKeyViewModel;
        private TestKeyStepViewModel testKeyViewModel;

        public CopyKeyViewModel(IEventAggregator eventAggregator)
        {
            this.eventAggregator = eventAggregator;
            this.eventAggregator.Subscribe(this);
            this.identifyKeyViewModel = new IdentifyKeyStepViewModel(this.eventAggregator);
            this.writeKeyViewModel = new WriteKeyStepViewModel(this.eventAggregator);
            this.testKeyViewModel = new TestKeyStepViewModel(this.eventAggregator);
            this.Items.Add(this.identifyKeyViewModel);
            this.Items.Add(this.writeKeyViewModel);
            this.Items.Add(this.testKeyViewModel);
        }


        protected override void OnActivate()
        {
            this.numberOfCopies = 0;
            this.ActivateStep(0);
        }

        protected override void OnDeactivate(bool close)
        {
            if (numberOfCopies > 0 && this.writeKeyViewModel.OriginalKey != null)
            {
                Tracker.TrackKey(this.writeKeyViewModel.OriginalKey.KeyType, numberOfCopies);
            }
            this.Reset();
        }

        public void OnBackToMain()
        {
            this.eventAggregator.PublishOnUIThread(new BackToMainMenuEvent() { Sender = this });
        }

        void IHandle<StepFinishedEvent>.Handle(StepFinishedEvent message)
        {
            if (message.StepNumber < 2)
            {
                if (message.StepNumber == 0)
                {
                    this.writeKeyViewModel.OriginalKey = this.identifyKeyViewModel.KeyInformation;
                    this.testKeyViewModel.OriginalKey = this.identifyKeyViewModel.KeyInformation;
                }
                if (message.StepNumber == 1)
                {
                    numberOfCopies++;
                }

                this.ActivateStep(message.StepNumber + 1);
            }
            else if (message.StepNumber == 2)
            {
                Tracker.TrackKey(this.writeKeyViewModel.OriginalKey.KeyType, numberOfCopies);
                this.Reset();
            }
        }

        private void Reset()
        {
            this.writeKeyViewModel.OriginalKey = null;
            this.testKeyViewModel.OriginalKey = null;
            this.numberOfCopies = 0;
            this.ActivateStep(0);
        }

        private void ActivateStep(int newStep)
        {
            this.SelectedStep = newStep;
            this.ActivateItem(this.Items[this.SelectedStep]);
        }

        public void Handle(CreateAnotherDuplicateEvent message)
        {
            this.ActivateStep(1);
        }

        public int SelectedStep
        {
            get
            {
                return selectedStep;
            }

            set
            {
                if (value == selectedStep)
                {
                    return;
                }

                selectedStep = value;

                this.NotifyOfPropertyChange();
            }

        }
    }
}
