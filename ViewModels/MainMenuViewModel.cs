﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleKeyCopy.ViewModels
{
    public class MainMenuViewModel : Screen
    {
        private IEventAggregator _eventAggregator;
        private System.Action _copyKeyAction;
        private System.Action _testKeyAction;


        public MainMenuViewModel(IEventAggregator eventAggregator, System.Action testKeyAction, System.Action copyKeyAction)
        {
            this._eventAggregator = eventAggregator;
            this._copyKeyAction = copyKeyAction;
            this._testKeyAction = testKeyAction;
        }

        public void OnCopyKey()
        {
            _copyKeyAction();
        }

        public void OnTestKey()
        {
            _testKeyAction();
        }
    }
}
