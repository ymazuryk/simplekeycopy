﻿using Caliburn.Micro;
using SimpleKeyCopy.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleKeyCopy.ViewModels
{
    public abstract class StepViewModel : Screen
    {
        protected IEventAggregator _eventAggregator;
        protected static NLog.ILogger logger = NLog.LogManager.GetCurrentClassLogger();

        public StepViewModel(int stepNumber, IEventAggregator eventAggregator)
        {
            this.StepNumber = stepNumber;
            this._eventAggregator = eventAggregator;
        }

        public int StepNumber { get; private set; }

        protected void PublishStepFinishedEvent()
        {
            this._eventAggregator.PublishOnUIThread(new StepFinishedEvent(this.StepNumber));
        }
    }
}
