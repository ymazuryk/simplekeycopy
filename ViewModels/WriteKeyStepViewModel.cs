﻿using Caliburn.Micro;
using SimpleKeyCopy.Proxmark;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SimpleKeyCopy.ViewModels
{
    public class WriteKeyStepViewModel : StepViewModel
    {
        private ProxmarkKeyInformation originalKey;

        public WriteKeyStepViewModel(IEventAggregator eventAggregator) : base(1, eventAggregator)
        {

        }

        protected override void OnActivate()
        {
            base.OnActivate();
            logger.Trace("WriteKey step activated");
        }

        public async void OnWriteNewKey()
        {
            if (MessageBox.Show("Place empty key on reader.\nPush OK when ready.",
                "Prepare empty key",
                MessageBoxButton.OKCancel,
                MessageBoxImage.Asterisk) == MessageBoxResult.OK)
            {
                logger.Trace($"Writing key {this.OriginalKey}");
                await ProxmarkManager.Instance.WriteKey(this.OriginalKey);
                this.PublishStepFinishedEvent();
            }
        }

        public ProxmarkKeyInformation OriginalKey
        {
            get
            {
                return originalKey;
            }

            set
            {
                if (value == originalKey)
                {
                    return;
                }

                originalKey = value;
                this.NotifyOfPropertyChange();
            }
        }
    }
}
