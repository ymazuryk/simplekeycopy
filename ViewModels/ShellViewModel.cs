using System;
using Caliburn.Micro;
using SimpleKeyCopy.Events;

namespace SimpleKeyCopy.ViewModels
{
    /// <summary>
    /// Shell view model is a conductor, which will have active item
    /// That will be either MainMenu, CopyKey or TestKey views
    /// </summary>
    public class ShellViewModel : Conductor<IScreen>.Collection.OneActive, 
                                    IShell,
                                    IHandle<BackToMainMenuEvent> 
    {
        private readonly MainMenuViewModel _mainMenuViewModel;
        private readonly TestKeyViewModel _testKeyViewModel;
        private readonly CopyKeyViewModel _copyKeyViewModel;
        private IEventAggregator _eventAggregator;

        public ShellViewModel(IEventAggregator eventAggregator)
        {
            DisplayName = "Simple Key Copy";
            _eventAggregator = eventAggregator;
            _eventAggregator.Subscribe(this);

            _testKeyViewModel = new TestKeyViewModel(_eventAggregator);
            _copyKeyViewModel = new CopyKeyViewModel(_eventAggregator);
            _mainMenuViewModel = new MainMenuViewModel(_eventAggregator, this.OnTestKey, this.OnCopyKey);
            this.ActivateItem(_mainMenuViewModel);            
        }

        void IHandle<BackToMainMenuEvent>.Handle(BackToMainMenuEvent message)
        {
            this.ActivateItem(_mainMenuViewModel);   
        }

        public void OnTestKey()
        {
            this.ActivateItem(_testKeyViewModel);
        }

        public void OnCopyKey()
        {
            this.ActivateItem(_copyKeyViewModel); 
        }
    }
}