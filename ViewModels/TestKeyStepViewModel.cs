﻿using Caliburn.Micro;
using SimpleKeyCopy.Events;
using SimpleKeyCopy.Proxmark;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleKeyCopy.ViewModels
{
    public class TestKeyStepViewModel:  StepViewModel
    {
        private ProxmarkKeyInformation duplicateKey;
        private ProxmarkKeyInformation originalKey;
        private bool? keyComparisonResult;

        public TestKeyStepViewModel(IEventAggregator eventAggregator): base(2, eventAggregator)
        {
            this.OriginalKey = null;
        }

        public async void OnReadKey()
        {
            this.DuplicateKey = await ProxmarkManager.Instance.LfSearch();
            this.KeyComparisonResult = this.OriginalKey.Equals(this.DuplicateKey);
        }

        public void OnFinish()
        {
            logger.Trace("Cloning finished");
            this.PublishStepFinishedEvent();
        }

        public void OnCreateAnotherDuplicate()
        {
            logger.Trace("Creating another duplicate");
            this._eventAggregator.PublishOnUIThread(new CreateAnotherDuplicateEvent());
        }

        protected override void OnActivate()
        {
            this.Reset();
            logger.Trace($"Test key step activated");
        }

        private void Reset()
        {
            this.DuplicateKey = null;
            this.KeyComparisonResult = null;
        }

        public ProxmarkKeyInformation OriginalKey
        {
            get
            {
                return originalKey;
            }

            set
            {
                if (ProxmarkKeyInformation.Equals(value, originalKey))
                {
                    return;
                }

                originalKey = value;
                this.NotifyOfPropertyChange();
            }
        }

        public bool? KeyComparisonResult
        {
            get
            {
                return this.keyComparisonResult;
            }

            set
            {
                if (value == this.keyComparisonResult)
                {
                    return;
                }

                this.keyComparisonResult = value;
                this.NotifyOfPropertyChange();
            }
        }

        public ProxmarkKeyInformation DuplicateKey
        {
            get
            {
                return duplicateKey;
            }

            set
            {
                if (ProxmarkKeyInformation.Equals(value, duplicateKey))
                {
                    return;
                }

                duplicateKey = value;
                this.NotifyOfPropertyChange();
            }
        }
    }
}
