﻿using Caliburn.Micro;
using SimpleKeyCopy.Events;
using SimpleKeyCopy.Proxmark;
using System.Windows;
using System;

namespace SimpleKeyCopy.ViewModels
{
    /// <summary>
    /// view model for the test key step
    /// </summary>
    public class TestKeyViewModel : Screen
    {
        private IEventAggregator eventAggregator;
        private ProxmarkKeyInformation key1Information;
        private ProxmarkKeyInformation key2Information;
        private bool? keyComparisonResult;

        public TestKeyViewModel(IEventAggregator _eventAggregator)
        {
            this.eventAggregator = _eventAggregator;
            this.Key1Information =null;
            this.Key2Information = null;
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            this.Reset();
        }
        public void OnBackToMain()
        {
            this.eventAggregator.PublishOnUIThread(new BackToMainMenuEvent() { Sender = this });
        }

        public bool CanScanKey2
        {
            get
            {
                return this.Key1Information != null;
            }
        }

        public bool? KeyComparisonResult
        {
            get
            {
                return this.keyComparisonResult;
            }

            set
            {
                if (value == this.keyComparisonResult)
                {
                    return;
                }

                this.keyComparisonResult = value;
                this.NotifyOfPropertyChange();
            }
        }

        public ProxmarkKeyInformation Key1Information
        {
            get
            {
                return this.key1Information;
            }

            set
            {
                if (ProxmarkKeyInformation.Equals(value, key1Information))
                {
                    return;
                }

                key1Information = value;
                this.NotifyOfPropertyChange();
                this.NotifyOfPropertyChange(nameof(CanScanKey2));
            }
        }

        public ProxmarkKeyInformation Key2Information
        {
            get
            {
                return this.key2Information;
            }

            set
            {
                if (ProxmarkKeyInformation.Equals(value, key2Information))
                {
                    return;
                }

                key2Information = value;
                this.NotifyOfPropertyChange();
            }
        }

        public async void OnScanKey1()
        {
            var keyInfo = await ProxmarkManager.Instance.LfSearch();
            if (keyInfo == null)
            {
                this.Key1Information = null;
                MessageBox.Show("No Key found, please try again", "No Key Found!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                this.Key1Information = keyInfo;
            }
        }

        public async void OnScanKey2()
        {
            var keyInfo = await ProxmarkManager.Instance.LfSearch();
            if (keyInfo == null)
            {
                this.Key2Information = null;
                MessageBox.Show("No Key found, please try again", "No Key Found!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                this.Key2Information = keyInfo;
                this.KeyComparisonResult = this.Key2Information.Equals(this.Key1Information);
            }
        }

        public void OnFinish()
        {
            this.Reset();
        }

        private void Reset()
        {
            this.IsNotifying = false;
            this.Key1Information = null;
            this.Key2Information = null;
            this.KeyComparisonResult = null;
            this.IsNotifying = true;
            this.Refresh();
        }
    }
}
