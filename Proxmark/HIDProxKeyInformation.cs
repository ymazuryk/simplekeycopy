﻿using System;

namespace SimpleKeyCopy.Proxmark
{
    public class HIDProxKeyInformation: ProxmarkKeyInformation
    {
        public HIDProxKeyInformation()
        {
            KeyType = KeyType.HIDProxKey;
        }

        public string TagID { get; set; }

        public int Bitness { get; set; }

        public override bool Equals(ProxmarkKeyInformation other)
        {
            var otherHid = other as HIDProxKeyInformation;

            if (otherHid == null)
            {
                return false;
            }

            return string.Equals(this.TagID, otherHid.TagID);
        }

        public override string ToString()
        {
            return $"{KeyType}:tagId={TagID}";
        }
    }
}
