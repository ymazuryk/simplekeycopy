﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleKeyCopy.Proxmark
{
    internal static class KeyCommandFactory
    {
        public static List<string> CreateWriteCommand(ProxmarkKeyInformation proxmarkKey)
        {
            if (proxmarkKey == null)
            {
                return null;
            }

            switch (proxmarkKey.KeyType)
            {
                case KeyType.AWIDKey:
                    return CreateAwidWriteCommand(proxmarkKey as AWIDKeyInformation);
                case KeyType.HIDProxKey:
                    return CreateHidProxWriteCommand(proxmarkKey as HIDProxKeyInformation);
                case KeyType.IndalaKey:
                    return CreateIndalaWriteCommand(proxmarkKey as IndalaKeyInformation);
                case KeyType.IOProxKey:
                    return CreateIOProxWriteCommand(proxmarkKey as IOProxKeyInformation);
                case KeyType.ParadoxKey:
                    return CreateParadoxWriteCommand(proxmarkKey as ParadoxKeyInformation);
                case KeyType.EM4100Key:
                    return CreateEM4100WriteCommand(proxmarkKey as EM4100KeyInformation);
                default:
                    return null;
            }
        }

        private static List<string> CreateEM4100WriteCommand(EM4100KeyInformation eM4100KeyInformation)
        {
            if (string.IsNullOrEmpty(eM4100KeyInformation?.Tag))
            {
                return null;
            }

            return new List<string> { $"lf em4x em410xwrite 1 {eM4100KeyInformation.Tag}" };
        }

        private static List<string> CreateParadoxWriteCommand(ParadoxKeyInformation paradoxKeyInformation)
        {
            var commands = new List<string>();

            commands.Add($"lf t55xx wr 0 {paradoxKeyInformation.Block0}");
            commands.Add($"lf t55xx wr 1 {paradoxKeyInformation.Block1}");
            commands.Add($"lf t55xx wr 2 {paradoxKeyInformation.Block2}");
            commands.Add($"lf t55xx wr 3 {paradoxKeyInformation.Block3}");

            return commands;
        }

        private static List<string> CreateIOProxWriteCommand(IOProxKeyInformation ioProxKeyInformation)
        {
            if (ioProxKeyInformation == null)
            {
                return null;
            }

            return new List<string> { $"lf io clone {ioProxKeyInformation.XSF}" };
        }

        private static List<string> CreateIndalaWriteCommand(IndalaKeyInformation indalaKeyInformation)
        {
            if (indalaKeyInformation == null)
            {
                return null;
            }

            return new List<string> { $"lf indalaclone {indalaKeyInformation.UID}" };
        }

        private static List<string> CreateHidProxWriteCommand(HIDProxKeyInformation hidProxyKeyInformation)
        {
            if (hidProxyKeyInformation == null)
            {
                return null;
            }

            return new List<string> { $"lf hid clone {hidProxyKeyInformation.TagID}" };
        }

        private static List<string> CreateAwidWriteCommand(AWIDKeyInformation awidKeyInformation)
        {
            if (awidKeyInformation == null)
            {
                return null;
            }

            return new List<string> { $"lf awid clone {awidKeyInformation.FC} {awidKeyInformation.Card}" };
        }
    }
}
