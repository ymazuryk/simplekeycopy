﻿namespace SimpleKeyCopy.Proxmark.Validators
{
    internal class EM4100Validator : KeyValidator
    {
        public EM4100Validator() : base(KeyType.EM4100Key)
        {
        }
    }
}