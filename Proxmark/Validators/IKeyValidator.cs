﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleKeyCopy.Proxmark.Validators
{
    public interface IKeyValidator
    {
       KeyValidationResult ValidateKey(ProxmarkKeyInformation key);
    }
}
