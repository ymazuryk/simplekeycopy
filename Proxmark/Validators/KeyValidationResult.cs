﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleKeyCopy.Proxmark.Validators
{
    public class KeyValidationResult
    {

        public KeyValidationResult()
        {
            this.IsValid = true;
        }

        private readonly List<string> validationErrorList = new List<string>();

        /// <summary>
        /// Indicates wheter validated key is ok or not
        /// </summary>
        public bool IsValid { get; set;}

        /// <summary>
        /// Lists all encountered validation errors for a key
        /// </summary>
        public List<string> ValidationErrors => this.validationErrorList;
    }
}
