﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleKeyCopy.Proxmark.Validators
{
    public abstract class KeyValidator : IKeyValidator
    {
        protected KeyType ValidKeyType { get; set;}

        protected KeyValidator(KeyType keyType)
        {
            this.ValidKeyType = keyType;
        }

        public static IKeyValidator Create(KeyType keyType) 
        {
            var type = KeyFactory.GetKeyType(keyType);
            if (type == null)
            {
                return null;
            }

            switch(keyType)
            {
                case KeyType.AWIDKey:
                    return new AWIDValidator();
                case KeyType.HIDProxKey:
                    return new HIDProxValidator();
                case KeyType.IndalaKey:
                    return new IndalaValidator();
                case KeyType.IOProxKey:
                    return new IOProxValidator();
                case KeyType.ParadoxKey:
                    return new ParadoxValidator();
                case KeyType.EM4100Key:
                    return new EM4100Validator();
                default:
                    return null;
            }
        }

        public virtual KeyValidationResult ValidateKey(ProxmarkKeyInformation key)
        {
            var validationResult = new KeyValidationResult();
            if (key.KeyType != this.ValidKeyType)
            {
                validationResult.IsValid = false;
                validationResult.ValidationErrors.Add($"Expected keyType is {ValidKeyType}, actual is {key.KeyType}");
            }

            var expectedType = KeyFactory.GetKeyType(key.KeyType);
            if (key.GetType() != expectedType)
            {
                validationResult.IsValid = false;
                validationResult.ValidationErrors.Add($"Expected object Type is {expectedType.Name}, actual is {key.GetType().Name}");
            }

            return validationResult;
        }
    }
}
