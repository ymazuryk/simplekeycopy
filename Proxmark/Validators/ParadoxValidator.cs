﻿namespace SimpleKeyCopy.Proxmark.Validators
{
    internal class ParadoxValidator : KeyValidator
    {
        public ParadoxValidator() : base(KeyType.ParadoxKey)
        {
        }
    }
}