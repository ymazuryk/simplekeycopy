﻿namespace SimpleKeyCopy.Proxmark.Validators
{
    internal class HIDProxValidator : KeyValidator
    {
        public HIDProxValidator() : base(KeyType.HIDProxKey)
        {
        }

        public override KeyValidationResult ValidateKey(ProxmarkKeyInformation key)
        {
            var result = base.ValidateKey(key);
            if (result.IsValid)
            {
                var hidProxKey = key as HIDProxKeyInformation;
                if (hidProxKey.Bitness != 26 && hidProxKey.Bitness != 36)
                {
                    result.IsValid = false;
                    result.ValidationErrors.Add($"HIDProx key is {hidProxKey.Bitness} bit long, should be 26 or 36");
                }
            }
            return result;
        }
    }
}