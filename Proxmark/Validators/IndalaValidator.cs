﻿namespace SimpleKeyCopy.Proxmark.Validators
{
    internal class IndalaValidator : KeyValidator
    {
        public IndalaValidator() : base(KeyType.IndalaKey)
        {
        }
    }
}