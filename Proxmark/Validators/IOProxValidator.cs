﻿namespace SimpleKeyCopy.Proxmark.Validators
{
    internal class IOProxValidator : KeyValidator
    {
        public IOProxValidator() : base(KeyType.IOProxKey)
        {
        }
    }
}