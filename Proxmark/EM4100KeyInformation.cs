﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleKeyCopy.Proxmark
{
    public class EM4100KeyInformation : ProxmarkKeyInformation
    {
        public EM4100KeyInformation()
        {
            this.KeyType = KeyType.EM4100Key;
        }

        public override bool Equals(ProxmarkKeyInformation other)
        {
            var otherEM4100Key = other as EM4100KeyInformation;
            if (otherEM4100Key == null)
            {
                return false;
            }

            return string.Equals(this.Tag, otherEM4100Key.Tag);
        }

        public string Tag { get; set; }
        public override string ToString()
        {
            return $"{KeyType}:Tag={Tag}";
        }
    }
}
