﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleKeyCopy.Proxmark
{
    public class IndalaKeyInformation: ProxmarkKeyInformation
    {
        public IndalaKeyInformation()
        {
            KeyType = KeyType.IndalaKey;
        }

        public override bool Equals(ProxmarkKeyInformation other)
        {
            var otherIndala = other as IndalaKeyInformation;

            if (otherIndala == null)
            {
                return false;
            }

            return string.Equals(this.UID, otherIndala.UID);
        }

        public string UID { get; set; }

        public override string ToString()
        {
            return $"{KeyType}:tagId={UID}";
        }
    }
}
