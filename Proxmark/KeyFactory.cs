﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace SimpleKeyCopy.Proxmark
{
    /// <summary>
    /// Using factory method in a separate class since the logic is quite complex
    /// This factory can only create keys from proxmark output, this dependency is 
    /// isolated here rather than brought into ProxmarkKeyInformation class
    /// </summary>
    public static class KeyFactory
    {
        private static string IndalaMarker = "indala uid=";
        private static string AwidMarker = "awid found";
        private static string IoProxMarker = "io prox xsf";
        private static string HidProxMarker = "hid prox";
        private static string EM4100Marker = "em tag id";
        private static string ParadoxMarker = "paradox tag id:";


        /// <summary>
        /// This is used purely for basic key validation
        /// </summary>
        private static Dictionary<KeyType, Type> keyTypeToClassType = new Dictionary<KeyType, Type>
        {
            { KeyType.AWIDKey, typeof(AWIDKeyInformation) },
            { KeyType.HIDProxKey, typeof(HIDProxKeyInformation) },
            { KeyType.IndalaKey, typeof(IndalaKeyInformation) },
            { KeyType.IOProxKey, typeof(IOProxKeyInformation) },
            { KeyType.ParadoxKey, typeof(ParadoxKeyInformation) },
            { KeyType.EM4100Key, typeof(EM4100KeyInformation) }
        };

        public static Type GetKeyType(KeyType keyType)
        {
            if (keyTypeToClassType.ContainsKey(keyType))
            {
                return keyTypeToClassType[keyType]; 
            }

            return null;
        }

        public static ProxmarkKeyInformation CreateKey(string[] proxmarkOutput)
        {
            if (proxmarkOutput.Length < 3)
            {
                return null;
            }

            var type = GetKeyType(proxmarkOutput[proxmarkOutput.Length - 3]);
            return CreateKey(type, proxmarkOutput);
        }

        private static ProxmarkKeyInformation CreateKey(KeyType type, string[] proxmarkOutput)
        {
            var ky = typeof(AWIDKeyInformation);
            switch (type)
            {
                case KeyType.IndalaKey:
                    return CreateIndalaKey(proxmarkOutput);
                case KeyType.AWIDKey:
                    return CreateAWIDKey(proxmarkOutput);
                case KeyType.HIDProxKey:
                    return CreateHIDProxKey(proxmarkOutput);
                case KeyType.IOProxKey:
                    return CreateIOProxKey(proxmarkOutput);
                case KeyType.ParadoxKey:
                    return CreateParadoxKey(proxmarkOutput);
                case KeyType.EM4100Key:
                    return CreateEM4100Key(proxmarkOutput);
                default:
                    return null;
            }
        }

        private static ProxmarkKeyInformation CreateEM4100Key(string[] proxmarkOutput)
        {
            var em4100Key = new EM4100KeyInformation();
            var keyString = proxmarkOutput.FirstOrDefault(s => s.ToLower().StartsWith(EM4100Marker));
            if (string.IsNullOrEmpty(keyString))
            {
                return null;
            }

            var keyValue = keyString.Trim().Split(':');
            if (keyValue?.Length != 2)
            {
                return null;
            }

            var tag = keyValue[1].Trim();
            em4100Key.Tag = tag;

            return em4100Key;
        }

        private static ProxmarkKeyInformation CreateParadoxKey(string[] proxmarkOutput)
        {
            var paradoxKey = new ParadoxKeyInformation();
            var keyString = proxmarkOutput.FirstOrDefault(s => s.ToLower().StartsWith(ParadoxMarker));

            if (string.IsNullOrEmpty(keyString))
            {
                return null;
            }

            string[] values = keyString.Trim().Split(' ');
            paradoxKey.RawKey = values[values.Length - 1].Trim();
            return paradoxKey;
        }

        private static ProxmarkKeyInformation CreateIndalaKey(string[] proxmarkOutput)
        {
            var keyString = proxmarkOutput.FirstOrDefault(s => s.ToLower().IndexOf(IndalaMarker) == 0);

            if (string.IsNullOrEmpty(keyString))
            {
                return null;
            }

            string[] keyInfo = keyString.Trim().Split(' ');

            var id = keyInfo[keyInfo.Length - 1];

            return new IndalaKeyInformation{ UID = id.Substring(1, id.Length - 2) };
        }

        private static ProxmarkKeyInformation CreateHIDProxKey(string[] proxmarkOutput)
        {
            var keyString = proxmarkOutput.FirstOrDefault(s => s.ToLower().IndexOf(HidProxMarker) == 0);
            if (string.IsNullOrEmpty(keyString))
            {
                return null;
            }
            int bitness = 0;

            var keyValuePairs = keyString.Trim().Split('-');

            foreach(var keyValuePair in keyValuePairs)
            {
                var keyValue = keyValuePair.Trim().Split(':');
                if (keyValue.Length == 2 && keyValue[0] == "Format Len")
                {
                    var bitnessString = keyValue[1];
                    var length = bitnessString.IndexOf("bit");
                    if (length > 0 && length < bitnessString.Length)
                    {
                        int.TryParse(bitnessString.Substring(0, length), out bitness);
                    }
                }
            }
            var keyInfo = keyString.Trim().Split(' ');
            if (keyInfo.Length < 5 || string.IsNullOrEmpty(keyInfo[4]))
            {
                return null;
            }

            return new HIDProxKeyInformation { TagID = keyInfo[4].Trim(), Bitness = bitness };
        }

        private static ProxmarkKeyInformation CreateAWIDKey(string[] proxmarkOutput)
        {
            var keyString = proxmarkOutput.FirstOrDefault(s => s.ToLower().IndexOf(AwidMarker) == 0);
            if (string.IsNullOrEmpty(keyString))
            {
                return null;
            }

            string[] keyInfo = keyString.Trim().Split(',');

            var fcInfo = keyInfo.FirstOrDefault(s => s.Trim().ToLower().IndexOf("fc") == 0);
            var cardInfo = keyInfo.FirstOrDefault(s => s.Trim().ToLower().IndexOf("card") == 0);
            if (string.IsNullOrEmpty(fcInfo) || string.IsNullOrEmpty(cardInfo))
            {
                return null;
            }

            var fc = fcInfo.Trim().Split(' ')[1].Trim();
            var card = cardInfo.Trim().Split(' ')[1].Trim();

            if (string.IsNullOrEmpty(fc) || string.IsNullOrEmpty(card))
            {
                return null;
            }

            return new AWIDKeyInformation{ FC = fc, Card = card };
        }

        private static ProxmarkKeyInformation CreateIOProxKey(string[] proxmarkOutput)
        {
            var keyString = proxmarkOutput.FirstOrDefault(s => s.ToLower().IndexOf(IoProxMarker) == 0);
            if (string.IsNullOrEmpty(keyString))
            {
                return null;
            }

            var keyInfo = keyString.Trim().Split(' ');

            if (keyInfo?.Length < 4)
            {
                return null;
            }

            var xsfInfo = keyInfo[3].Trim();
            if (string.IsNullOrEmpty(xsfInfo))
            {
                return null;
            }

            return new IOProxKeyInformation() { XSF = xsfInfo.Substring(1, xsfInfo.Length -2) };
        }

        private static KeyType GetKeyType(string proxmarkOutputString)
        {
            if (proxmarkOutputString.ToLower().Contains("hid prox"))
            {
                return KeyType.HIDProxKey;
            }

            if (proxmarkOutputString.ToLower().Contains("io prox"))
            {
                return KeyType.IOProxKey;
            }

            if (proxmarkOutputString.ToLower().Contains("indala"))
            {
                return KeyType.IndalaKey;
            }

            if (proxmarkOutputString.ToLower().Contains("awid"))
            {
                return KeyType.AWIDKey;
            }

            if(proxmarkOutputString.ToLower().Contains("paradox"))
            {
                return KeyType.ParadoxKey;
            }

            if (proxmarkOutputString.ToLower().Contains("valid em410"))
            {
                return KeyType.EM4100Key;
            }

            return KeyType.Unknown;
        }
    }
}
