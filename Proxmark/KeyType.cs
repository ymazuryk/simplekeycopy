﻿namespace SimpleKeyCopy.Proxmark
{
    public enum KeyType
    {
        HIDProxKey,
        IOProxKey,
        IndalaKey,
        AWIDKey,
        ParadoxKey,
        EM4100Key,
        Unknown
    }
}
