﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleKeyCopy.Proxmark
{
    public class AWIDKeyInformation: ProxmarkKeyInformation
    {
        public AWIDKeyInformation()
        {
            KeyType = KeyType.AWIDKey;
        }

        public override bool Equals(ProxmarkKeyInformation other)
        {
            var otherAwid = other as AWIDKeyInformation;

            if (otherAwid == null)
            {
                return false;
            }

            return string.Equals(this.FC, otherAwid.FC) && string.Equals(this.Card, otherAwid.Card);
        }

        public string FC { get; set; }

        public string Card { get; set; }

        public override string ToString()
        {
            return $"{KeyType}:FC={FC}, Card={Card}";
        }
    }
}
