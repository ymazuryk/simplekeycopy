﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using System.Windows;
using System.Collections.Generic;

namespace SimpleKeyCopy.Proxmark
{
    public class ProxmarkManager
    {
        private static Lazy<ProxmarkManager> instance = new Lazy<ProxmarkManager>();
        // 
        private static string executablePath = "pm3-bin-2.2.0\\win32 (client+GUI)\\go.bat";
        //private static string executablePath = "proxmark3stub\\proxmark3stub.exe";

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private Process proxmarkProcess;
        private List<string> currentOutput = new List<string>();

        private AutoResetEvent outputReadyFlag = new AutoResetEvent(false);
        private AutoResetEvent writeCompleted = new AutoResetEvent(false);
        private AutoResetEvent listenerStarted = new AutoResetEvent(false);
        public ProxmarkManager()
        {

        }

        public static ProxmarkManager Instance => instance.Value;

        private void Start()
        {
            if (proxmarkProcess != null)
            {
                throw new InvalidOperationException("Proxmark already running");
            }

            var currentPath = Directory.GetCurrentDirectory();
            proxmarkProcess = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = Path.Combine(currentPath, executablePath),
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    WorkingDirectory = Path.GetDirectoryName(executablePath),
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true
                }
            };
            proxmarkProcess.Start();
        }

        public async Task WriteKey(ProxmarkKeyInformation proxmarkKey)
        {
            logger.Trace($"Copying key {proxmarkKey}...");
            currentOutput.Clear();
            this.Start();
            Task.Factory.StartNew(() => CloneOutputReader(proxmarkKey.KeyType));
            logger.Trace("Waiting for proxmark to start..");
            var commands = KeyCommandFactory.CreateWriteCommand(proxmarkKey);
            listenerStarted.WaitOne();
            logger.Trace("Proxmark started, ready to run commands");
            foreach (var command in commands)
            {
                if (command != null)
                {
                    logger.Trace($"Trying to execute command: {command}");
                    proxmarkProcess.StandardInput.WriteLine(command);
                }
                Thread.Sleep(10);
                if (!writeCompleted.WaitOne(5000))
                {
                    MessageBox.Show("Cloning status unknown\nNo valid response from Proxmark\nPlease test the key to verify if cloning succeeded", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            proxmarkProcess.StandardInput.WriteLine("exit");

            outputReadyFlag.WaitOne(10000);

            logger.Trace($"proxmark3 output\r\n:{this.GetOutputString()}\r\n---------------------------------");
            proxmarkProcess = null;
        }

        private object GetOutputString()
        {
            return string.Join("\n\t", this.currentOutput);
        }

        private void OnOutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            var output = e.Data;
            logger.Trace($"on data received: {e.Data}");
        }


        public async Task<ProxmarkKeyInformation> LfSearch(string dataFileName = null)
        {
            logger.Trace("Running lf search command");
            this.Start();
            if (!string.IsNullOrEmpty(dataFileName))
            {
                proxmarkProcess.StandardInput.WriteLine(string.Format("data load {0}", dataFileName));
            }
            currentOutput.Clear();
            Task.Factory.StartNew(this.LfSearchOutputReader);
            proxmarkProcess.StandardInput.WriteLine("lf search");

            proxmarkProcess.StandardInput.WriteLine("exit");

            outputReadyFlag.WaitOne(10000);

            proxmarkProcess = null;

            logger.Trace($"proxmark3 output\r\n:{this.GetOutputString()}");
            return this.FindKey(string.Join("\n", this.currentOutput));
        }

        private ProxmarkKeyInformation FindKey(string processOutput)
        {
            var startIndex = processOutput.IndexOf("lf search") + 10;
            var endIndex = processOutput.IndexOf("exit");
            if (endIndex <= startIndex || startIndex < 0 || endIndex < 0)
            {
                return null;
            }

            string[] lines = processOutput.Substring(startIndex, endIndex - startIndex).Split('\n');
            return KeyFactory.CreateKey(lines);
        }

        private void Stop()
        {
            proxmarkProcess?.Close();
            proxmarkProcess = null;
        }

        private void LfSearchOutputReader()
        {
            var line = this.proxmarkProcess.StandardOutput.ReadLine();
            while (line != null)
            {
                currentOutput.Add(line);
                line = this.proxmarkProcess.StandardOutput.ReadLine();
            }
        
            outputReadyFlag.Set();
        }

        private void CloneOutputReader(KeyType keyType)
        {
            this.listenerStarted.Set();
            logger.Trace("Waiting for proxmark output..");
            var line = this.proxmarkProcess.StandardOutput.ReadLine();
            while (line != null)
            {
                logger.Trace($"Proxmark output received: {line}");
                currentOutput.Add(line);
                if (CloneFinished(line, keyType))
                {
                    writeCompleted.Set();
                }
                logger.Trace("No end of execution marker found for the command");
                line = this.proxmarkProcess.StandardOutput.ReadLine();
            }

            outputReadyFlag.Set();
        }

        private static bool CloneFinished(string output, KeyType type)
        {
            if (string.IsNullOrEmpty(output))
            {
                return false;
            }

            switch (type)
            {
                case KeyType.HIDProxKey:
                case KeyType.IOProxKey:
                case KeyType.IndalaKey:
                    return output.ToLower().IndexOf("#db# done!") >= 0;

                case KeyType.AWIDKey:
                    return output.ToLower().IndexOf("block 3") >= 0;

                case KeyType.ParadoxKey:
                    return output.ToLower().IndexOf("writing to block") >= 0;
                default:
                    return false;
            }
        }
    }
}
