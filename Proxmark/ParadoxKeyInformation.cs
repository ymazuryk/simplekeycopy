﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleKeyCopy.Proxmark
{
    public class ParadoxKeyInformation : ProxmarkKeyInformation
    {
        public string RawKey { get; set; }

        public ParadoxKeyInformation()
        {
            this.KeyType = KeyType.ParadoxKey;
        }
        public override bool Equals(ProxmarkKeyInformation other)
        {
            var paradoxKey = other as ParadoxKeyInformation;
            if (paradoxKey == null)
            {
                return false;
            }

            return (string.Equals(this.RawKey, paradoxKey.RawKey));
        }

        public string Block0 => "00107060";

        public string Block1
        {
            get
            {
                if (string.IsNullOrEmpty(RawKey))
                {
                    return null;
                }

                return RawKey.Substring(0, RawKey.Length - 16);
            }
        }

        public string Block2
        {
            get
            {
                if (string.IsNullOrEmpty(RawKey))
                {
                    return null;
                }

                return RawKey.Substring(RawKey.Length - 16, 8);
            }
        }

        public string Block3
        {
            get
            {
                if (string.IsNullOrEmpty(RawKey))
                {
                    return null;
                }

                return RawKey.Substring(RawKey.Length - 8, 8);
            }
        }

        public override string ToString()
        {
            return $"{KeyType}: Raw:{RawKey} Block0: {Block0} Block1: {Block1} Block2: {Block2} Block3: {Block3}";
        }

    }
}
