﻿using System;

namespace SimpleKeyCopy.Proxmark
{
    public class IOProxKeyInformation: ProxmarkKeyInformation
    {
        public IOProxKeyInformation()
        {
            KeyType = KeyType.IOProxKey;
        }

        
        public string XSF { get; set; }

        public override bool Equals(ProxmarkKeyInformation other)
        {
            var otherIoProx = other as IOProxKeyInformation;

            if (otherIoProx == null)
            {
                return false;
            }

            return string.Equals(this.XSF, otherIoProx.XSF);
        }

        public override string ToString()
        {
            return $"{KeyType}:tagId={XSF}";
        }
    }
}
