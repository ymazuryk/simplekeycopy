﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleKeyCopy.Proxmark
{
    public abstract class ProxmarkKeyInformation: IEquatable<ProxmarkKeyInformation>
    {
        public KeyType KeyType { get; set; }

        public abstract bool Equals(ProxmarkKeyInformation other);
    }
}
