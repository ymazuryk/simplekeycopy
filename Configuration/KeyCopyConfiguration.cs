﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleKeyCopy.Configuration
{
    public class KeyCopyConfiguration
    {
        private const string ProxmarkDirKey = "ProxmarkDir";

        private const string DefaultProxmarkDir = "pm3-bin-2.2.0\\win32 (client+GUI)\\";

        private const string CopyHistoryKey = "CopyHistory";

        private const string DefaultCopyHistory = "history.csv";

        private static Lazy<KeyCopyConfiguration> instance = new Lazy<KeyCopyConfiguration>(LoadConfiguration);

        public static KeyCopyConfiguration Instace =>  instance.Value;

        private static KeyCopyConfiguration LoadConfiguration()
        {
            var configuration = new KeyCopyConfiguration();
            var appConfiguration = ConfigurationManager.AppSettings;

            configuration.CopyHistoryFile = appConfiguration.AllKeys.Contains(CopyHistoryKey) 
                ? appConfiguration[CopyHistoryKey] 
                : DefaultCopyHistory;

            configuration.ProxmarkDir = appConfiguration.AllKeys.Contains(ProxmarkDirKey)
                ? appConfiguration[ProxmarkDirKey]
                : DefaultProxmarkDir;

            return configuration;
        }

        public string CopyHistoryFile { get; private set; }

        public string ProxmarkDir { get; private set; }
    }
}
