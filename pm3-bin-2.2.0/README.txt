========
| ROOT |
========
Contains useful files:

README.txt				(this file)
UpdateBootloader.pdf	(explains how to update bootloader from old libusb communication method to new CDC/serial one)


=================
| \firmware_win |
=================
Contains OS/FPGA and bootrom images to be flashed on Proxmark3 [see https://github.com/Proxmark/proxmark3/wiki/compiling for further information]:

fullimage.elf 			(full image - flash over USB)
\bootrom\bootrom.elf	(bootrom image - flash over USB)
\JTAG Only				(use bin files in that folder only with JTAG interface - needed only to flash proxmark3 from scratch if blank or if USB/bootloader is not working)		


=======================
| \win32 (client+GUI) |
=======================
Contains the Windows executables and batch files:

\lualibs												(contains needed files to run .lua scripts)
\scripts												(contains .lua scripts)
default_keys.dic										(mifare default Keys as already in Proxmark.exe)
Dump available client commands to h.txt and m.txt.bat	(write to text files all the available client commands in 2 different style format)   
FLASH - Bootrom.bat										(flashes the bootrom.elf file into bootloader)
FLASH - fullimage.bat									(flashes the fullimage.elf file)
FLASH - OS.bat											(flashes the OS.elf file as PM3 firmware)
Go.bat													(execute proxmark3.exe specifying the com port)
flasher.exe												(new flasher binary)
flasher-OLD.exe											(old flasher binary to use with PM3 with old bootloader - read UpdateBootloader.pdf for further information)
FLASH - NEW Bootrom (uses old flasher exe).bat			(flashes the new bootrom.elf file into bootloader using old flasher.exe - requires PM3 with old bootloader - read pdateBootloader.pdf for further information)
*.dll													(files needed to use above binaries in a stand-alone way)
*.py													(files needed to use above binaries in a stand-alone way)
proxmark3.exe											(proxmark3 client)
Proxmark Tool.*											(proxmark3 client Graphical User Interface [GUI])
settings.xml											(proxmark3 client GUI tree-view-command settings)


====================
| \Windows Drivers |
====================
Contains proxmark3 drivers:

\CDC (new serial interface) 			(new Serial drivers for proxmark3 bootrom from r655 (included r655))
\libusb-win32 (old usb interface) 		(old LibUSB drivers for proxmark3 bootrom previous to r655 (excluded r655))




Please, refere to http://proxmark.org/ and http://proxmark.org/forum/index.php for further details