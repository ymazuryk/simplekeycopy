﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleKeyCopy.Events
{
    public class StepFinishedEvent
    {
        public StepFinishedEvent(int stepNumber)
        {
            this.StepNumber = stepNumber;
        }

        public int StepNumber { get; set; }
    }
}
