﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace proxmark3stub
{
    class Program
    {
        static void Main(string[] args)
        {
            string input = null;

            while (!string.Equals(input, "exit"))
            {
                Console.Write("proxmark3>");
                input = Console.ReadLine();
                ProcessCommand(input);
            }
        }

        private static void ProcessCommand(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return;
            }

            if (input.IndexOf("lf hid clone") == 0)
            {
                Console.WriteLine("\n#db# DONE!");
            }

            if (input.IndexOf("lf io clone") == 0)
            {
                Console.WriteLine("\n#db# DONE!");
            }

            if (input.IndexOf("lf indala clone") == 0)
            {
                Console.WriteLine("\n#db# DONE!");
            }

            if (input.IndexOf("lf awid clone") == 0)
            {
                Console.WriteLine("Preparing to clone AWID26 to T55x7 with FC: 15, CN: 259 (Raw:011d817d1181711111111111)");
                Console.WriteLine("Block 0: 0x00107060");
                Console.WriteLine("Block 1: 0x011d817d");
                Console.WriteLine("Block 2: 0x11817111");
                Console.WriteLine("Block 3: 0x11111111");
            }
        }
    }
}
