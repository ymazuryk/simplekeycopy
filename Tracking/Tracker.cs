﻿using SimpleKeyCopy.Proxmark;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using SimpleKeyCopy.Configuration;
using System.IO;

namespace SimpleKeyCopy.Tracking
{
    public class Tracker
    {
        public static void TrackKey(KeyType keyType, int numberOfCopies = 1)
        {
            var fileName = KeyCopyConfiguration.Instace.CopyHistoryFile;
            using (var writer = new StreamWriter(fileName, true))
            {
                writer.WriteLine($"{DateTime.Now},{keyType},{numberOfCopies}");
            }
        }
    }
}
